package su.t1oo1.courcera.algorythms.week1

import org.scalatest.{Tag, ShouldMatchers, FunSpec}

class ConnectionTest extends FunSpec with ShouldMatchers {



  describe("Quick find") {
    it ("connects right elements") {
      val te = new TestEnv {
        override def connector: Connect = new QuickFind(arr)
      }
      te should be('rightConnections)
    }
  }

  describe("Quick union") {
    it ("connects right elements", Tag("integration")) {
      val te = new TestEnv {
        override def connector: Connect = new QuickUnion(arr)
      }
      te should be('rightConnections)
    }
  }

  describe("Quick union weighted") {
    it ("connects right elements") {
      val te = new TestEnv {
        override def connector: Connect = new QuickUnionWeighted(arr)
      }
      te should be('rightConnections)
    }
  }

  trait TestEnv {
    val steps = List(4 -> 3, 3 -> 8, 6 -> 5, 9 -> 4, 2 -> 1)

    val arr = (for {
      i <- 0 until 10
    } yield i).toArray

    def connector: Connect

    def isRightConnections = {
      val c = connector
      c.unionBySteps(steps)
      c.connected(8, 9) && !c.connected(5, 0)
    }
  }
}
