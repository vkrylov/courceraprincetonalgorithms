package su.t1oo1.courcera.algorythms.week1

import org.scalatest.{FlatSpec, ShouldMatchers}

class OrderTest extends FlatSpec with ShouldMatchers {

  trait Fixture {
    val steps = List(4 -> 3, 3 -> 8, 6 -> 5, 9 -> 4, 2 -> 1, 5 -> 0, 7 -> 2, 6 -> 1, 7 -> 3)
    val arr = (for {
      i <- 0 until 10
    } yield i).toArray

    def doSteps(connect: Connect) = {
      connect.unionBySteps(steps)
      connect.toList
    }
  }


  "Quick find" should "have the right order after steps" in new Fixture {
    doSteps(new QuickFind(arr)) should be(List(8, 8, 8, 8, 8, 8, 8, 8, 8, 8))
  }

  "Quick union" should "have the right order after steps" in new Fixture {
    doSteps(new QuickUnion(arr)) should be(List(0, 1, 1, 8, 3, 0, 1, 8, 8, 8))
  }

  "Quick union weighted" should "have the right order after steps" in new Fixture {
    doSteps(new QuickUnionWeighted(arr)) should be(List(6, 2, 6, 4, 6, 6, 6, 2, 4, 4))
  }
}
