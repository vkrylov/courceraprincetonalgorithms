package su.t1oo1.courcera.algorythms.week2

import org.scalatest.FunSuite
import su.t1oo1.courcera.algorythms.week1.QuickFind

class SortTest extends FunSuite {

  def mixedArray = List(2,4,6,7,8,9,0,1,3,5).toArray

  val sortedArray = List(0,1,2,3,4,5,6,7,8,9).toArray

  val assertSorted = assertResult(sortedArray)(_)

  test ("Selection sort should work correctly") {
    val arr = mixedArray
    new SelectionSort().sort(arr)
    assertSorted(arr)
  }

  test ("Insertion sort should work correctly") {
    val arr = mixedArray
    new InsertionSort().sort(arr)
    assertSorted(arr)
  }

  test ("Shell sort should work correctly") {
    val arr = mixedArray
    new ShellSort().sort(arr)
    assertSorted(arr)
  }
}