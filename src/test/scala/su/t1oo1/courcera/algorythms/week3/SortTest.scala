package su.t1oo1.courcera.algorythms.week3

import org.scalatest.FunSuite
import su.t1oo1.courcera.algorythms.week2.Sorting

trait SortTest extends FunSuite {

  test("Sort backward order") {
    assertSort(List(1, 2, 3, 4, 5, 6), Array(6, 5, 4, 3, 2, 1))
  }

  test("Sort forward order") {
    assertSort(List(1, 2, 3, 4, 5, 6), Array(1, 2, 3, 4, 5, 6))
  }

  test("Sort one number") {
    assertSort(List(1, 1, 1, 1), Array(1, 1, 1, 1))
  }

  test("Sort one element") {
    assertSort(List(1), Array(1))
  }

  test("Sort empty array") {
    assertSort(List(), Array[Int]())
  }

  test("Sort odd element count array") {
    assertSort(List(1, 2, 3), Array(1, 3, 2))
  }

  test("Sort two elements") {
    assertSort(List(2, 3), Array(3, 2))
  }

  def assertSort(expected: List[Int], initial: Array[Int]): Unit = {
    sorter.sort(initial)
    assertResult(expected)(initial.toList)
  }

  def sorter: Sorting
}

class MergeSortTest extends SortTest {
  override def sorter: Sorting = new MergeSort
}

class BottomUpMergeSortTest extends SortTest {
  override def sorter: Sorting = new BottomUpMergeSort
}

class QuickSortTest extends SortTest {
  override def sorter: Sorting = new QuickSort
}

