package su.t1oo1.courcera.algorythms.week3

import org.scalatest.FunSuite

class PartitionTest extends FunSuite {

  test("Partition right way") {
    val arr = Array(2, 1, 0, 3, -1, 5, 4)
    new QuickSort().sort(arr)
    assertResult(List(-1, 0, 1, 2, 3, 4, 5))(arr.toList)
  }

  test("Partition 2 item list") {
    val arr = Array(2, 1)
    new QuickSort().sort(arr)
    assertResult(List(1, 2))(arr.toList)
  }

  test("Partition 3 item list") {
    val arr = Array(2, 1, 3)
    new QuickSort().sort(arr)
    assertResult(List(1, 2, 3))(arr.toList)
  }

  test("Partition sorted list") {
    val arr = Array(1, 2, 3)
    new QuickSort().sort(arr)
    assertResult(List(1, 2, 3))(arr.toList)
  }

  test("Partition middle list") {
    val arr = Array(-1, 1, 0, 2)
    new QuickSort().sort(arr)
    assertResult(List(-1, 0, 1, 2))(arr.toList)
  }
}
