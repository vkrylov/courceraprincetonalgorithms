package su.t1oo1.courcera.algorythms.week3

import org.scalatest.FunSuite

class MergingTest extends FunSuite {

  test("Merge similar two parts") {
    val arr = Array(2, 4, 6, 1, 3, 5)
    new Mock().merge(arr)(0, arr.size)
    assertResult(List(1, 2, 3, 4, 5, 6))(arr.toList)
  }

  test("Merge small and big parts") {
    val arr = Array(1, 2, 3, 4, 5, 6)
    new Mock().merge(arr)(0, arr.size)
    assertResult(List(1, 2, 3, 4, 5, 6))(arr.toList)
  }

  test("Merge big and small parts") {
    val arr = Array(4, 5, 6, 1, 2, 3)
    new Mock().merge(arr)(0, arr.size)
    assertResult(List(1, 2, 3, 4, 5, 6))(arr.toList)
  }

  test("Merge same parts") {
    val arr = Array(1, 2, 3, 1, 2, 3)
    new Mock().merge(arr)(0, arr.size)
    assertResult(List(1, 1, 2, 2, 3, 3))(arr.toList)
  }

  test("Merge parts with single number") {
    val arr = Array(1, 1, 1, 1)
    new Mock().merge(arr)(0, arr.size)
    assertResult(List(1, 1, 1, 1))(arr.toList)
  }

  test("Merge part") {
    val arr = Array(2, 4, 6, 1, 3, 5)
    new Mock().merge(arr)(2, 4)
    assertResult(List(2, 4, 1, 6, 3, 5))(arr.toList)
  }

  test("Merge outbound part") {
    val arr = Array(1, 3, 4, 5, 2)
    new Mock().merge(arr)(0, 8)
    assertResult(List(1, 2, 3, 4, 5))(arr.toList)
  }
}

class Mock extends Merging {

}
