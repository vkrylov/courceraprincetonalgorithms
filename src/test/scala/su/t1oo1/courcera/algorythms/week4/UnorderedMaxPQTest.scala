package su.t1oo1.courcera.algorythms.week4

import org.scalatest.FunSuite

class UnorderedMaxPQTest extends FunSuite {

  test("scenario test") {
    val pq = new UnorderedMaxPQ[Char](8)
    pq.insert('P')
    pq.insert('Q')
    pq.insert('E')

    assert(pq.delMax() === 'Q')
    assert(!pq.empty())

    pq.insert('X')
    pq.insert('A')
    pq.insert('M')

    assert(pq.delMax() === 'X')
    assert(!pq.empty())

    pq.insert('P')
    pq.insert('L')
    pq.insert('E')

    assert(pq.delMax() === 'P')
    assert(!pq.empty())
  }

}
