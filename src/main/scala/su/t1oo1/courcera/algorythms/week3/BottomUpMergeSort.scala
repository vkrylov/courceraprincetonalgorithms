package su.t1oo1.courcera.algorythms.week3

import su.t1oo1.courcera.algorythms.sort.ui.{TriangleMarker, ProcessView}
import su.t1oo1.courcera.algorythms.week2.Sorting

class BottomUpMergeSort extends ProcessView with Sorting with Merging {

  override def sort(arr: Array[Int]): Unit = {
    mergePartial(arr)(2)
  }

  def mergePartial(arr: Array[Int])(partSize: Int): Unit = {
    var lastStep = 0
    var i = 0
    do {
      i += partSize
      merge(arr)(lastStep, i)
      show(arr, (lastStep until (if (arr.size < i) arr.size else i)).toList, TriangleMarker(lastStep, i - 1))
      lastStep = i
    } while (i < arr.size)
    if (partSize < arr.size)  mergePartial(arr)(partSize * 2)
  }
}
