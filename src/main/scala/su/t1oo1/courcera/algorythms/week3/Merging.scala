package su.t1oo1.courcera.algorythms.week3


trait Merging {

  def middle(from: Int, to: Int) = ((to - from) / 2) + from

  def merge(arr: Array[Int])(from: Int, to: Int): Unit = {
    val limit = if (arr.size < to) arr.size else to
    var a = from
    val m = middle(from, to)
    var b = m

    val sorted = for (i <- from until limit) yield {
      if (a >= m) {
        val bItem = arr(b)
        b += 1
        bItem
      } else if (b >= limit) {
        val aItem = arr(a)
        a += 1
        aItem
      } else {
        val aItem = arr(a)
        val bItem = arr(b)
        if (aItem < bItem) {
          a = a + 1
          aItem
        } else {
          b = b + 1
          bItem
        }
      }
    }

    for (i <- 0 until sorted.size) {
      arr(i + from) = sorted(i)
    }
  }
}
