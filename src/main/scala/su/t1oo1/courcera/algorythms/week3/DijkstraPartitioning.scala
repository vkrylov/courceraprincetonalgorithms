package su.t1oo1.courcera.algorythms.week3

import su.t1oo1.courcera.algorythms.sort.ui.{TextMarker, ProcessView}
import su.t1oo1.courcera.algorythms.week2.{Sorting, Swapping}

/**
 * Dijkstra 3way partitioning. Quick sort algorithm for data with duplicating values
 */
class DijkstraPartitioning extends ProcessView with Swapping[Int] with Sorting {

  override def sort(arr: Array[Int]): Unit = {
    val swap = swapItems(arr)(_, _)
    def partition(from: Int, to: Int): Unit = {
      if (from >= to - 1) return

      var lt = from // left marker index
      val v = arr(lt) //etalon
      var i = from // cursor
      val hi = to - 1
      var gt = hi // right marker index

      while (i <= gt) {
        val iVal = arr(i)

        if (v > iVal) {
          show(arr, i :: from :: lt :: Nil, TextMarker("i" -> i, "lt" -> lt, "gt" -> gt, "v" -> from, "hi" -> hi))
          swap(lt, i)
          i += 1
          lt += 1
        } else if (v < iVal) {
          show(arr, i :: gt :: Nil, TextMarker("i" -> i, "lt" -> lt, "gt" -> gt, "v" -> from, "hi" -> hi))
          swap(gt, i)
          gt -= 1
        } else {
          i += 1
        }
        show(arr, Nil, TextMarker("i" -> i, "lt" -> lt, "gt" -> gt, "v" -> from, "hi" -> hi))
      }

      partition(from, lt + 1)
      partition(i, to)
    }

    if (arr.size > 1) {
      partition(0, arr.size)
    }
  }

}