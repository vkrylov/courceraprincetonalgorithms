package su.t1oo1.courcera.algorythms.week3

import su.t1oo1.courcera.algorythms.sort.ui.{TriangleMarker, ProcessView}
import su.t1oo1.courcera.algorythms.week2.Sorting

import scala.annotation.tailrec


class MergeSort extends ProcessView with Sorting with Merging {

  override def sort(arr: Array[Int]): Unit = {
    def sortPart(from: Int, to: Int): Unit = {
      val delta = to - from
      delta match {
        case s if s >= 2 =>
          val m = middle(from, to)

          show(arr, Nil, TriangleMarker(from, m, to - 1))
          sortPart(from, m)
          sortPart(m, to)

          merge(arr)(from, to)
        case _ =>
      }
    }

    sortPart(0, arr.size)
  }

  override def merge(arr: Array[Int])(from: Int, to: Int): Unit = {
    super.merge(arr)(from, to)
    show(arr, (from until to).toList, TriangleMarker(from, to - 1))
  }
}
