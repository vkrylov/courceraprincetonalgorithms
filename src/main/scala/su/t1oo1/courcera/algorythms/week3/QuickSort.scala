package su.t1oo1.courcera.algorythms.week3

import su.t1oo1.courcera.algorythms.sort.ui.{ProcessView, TextMarker}
import su.t1oo1.courcera.algorythms.week2.{Sorting, Swapping}

/**
 * Not stable sorting algorithm. Split data by etalon value (usual first item value) into less item part and bigger or equals one,
 * sort recursively that parts in the same manner
 */
class QuickSort extends ProcessView with Swapping[Int] with Sorting {

  override def sort(arr: Array[Int]): Unit = {
    val swap = swapItems(arr)(_, _)
    def partition(from: Int, to: Int): Unit = {
      if (from >= to - 1) return
      val p = arr(from) // comparison value
      var i = from + 1 // left marker index
      var j = to - 1 // right marker index

      // do while marker indices are different
      while (i <= j) {
        show(arr, Nil, TextMarker("P" -> from, "L" -> (to - 1), "i" -> i, "j" -> j))
        val x = arr(i) // left marker value
        val y = arr(j) // right marker value
        x > p match {
          case true =>
            y >= p match {
              case true => j -= 1 // right marker is less than etalon -> decrease index
              case false => // swap when left marker is bigger than right
                show(arr, from :: i :: j :: Nil, TextMarker("P" -> from, "L" -> (to - 1), "i" -> i, "j" -> j))
                swap(i, j)
            }
          case false => i += 1 // left marker is less than etalon -> decrease index
        }
      }

      if (i <= to) {
        show(arr, from ::  j :: Nil, TextMarker("P" -> from, "L" -> (to - 1), "i" -> i, "j" -> j))
        swap(from, j)
      }
      partition(from, j)
      partition(j + 1, to )
    }

    partition(0, arr.size)
  }
}
