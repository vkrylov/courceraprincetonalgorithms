package su.t1oo1.courcera.algorythms.week2

import su.t1oo1.courcera.algorythms.sort.ui.{TriangleMarker, ProcessView}

class SelectionSort extends ProcessView with Swapping[Int] with Sorting {

  override def sort(arr: Array[Int]) {
    val swap = swapItems(arr)(_, _)

    for (i <- 0 until arr.size) {
      var min = arr(i)
      var minIndex = i
      for (j <- i until arr.size) {
        val current = arr(j)
        min = if (current < min) {
          minIndex = j
          current
        } else min

        show(arr, Nil, TriangleMarker(j, minIndex, i))
      }
      swap(i, minIndex)
      show(arr, i :: minIndex :: Nil, TriangleMarker(i))
    }
  }
}
