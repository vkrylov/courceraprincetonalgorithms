package su.t1oo1.courcera.algorythms.week2

trait Swapping[A] {

  def swapItems(arr: Array[A])(a: Int, b: Int) {
    val temp = arr(a)
    arr(a) = arr(b)
    arr(b) = temp
  }
}
