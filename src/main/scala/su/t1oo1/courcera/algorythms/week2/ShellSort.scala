package su.t1oo1.courcera.algorythms.week2

import su.t1oo1.courcera.algorythms.sort.ui.{TriangleMarker, ProcessView}

class ShellSort extends ProcessView with Swapping[Int] with Sorting {

  override def sort(arr: Array[Int]): Unit = {
    val N = arr.size

    val swap = swapItems(arr)(_, _)

    var h = 1
    while (h < (N / 3)) h = 3 * h + 1

    while (h >= 1) {
      for (i <- h until N) {
        var j = i
        while (j >= h && (arr(j) < arr(j - h)) ) {
          swap(j, j - h)
          show(arr, j :: (j - h) :: Nil, TriangleMarker(j, j - h, i))
          j = j - h
        }
        show(arr, Nil, TriangleMarker(j, i))
      }
      h = h / 3
    }
  }
}
