package su.t1oo1.courcera.algorythms.week2

import su.t1oo1.courcera.algorythms.sort.ui.{TriangleMarker, ProcessView}

class InsertionSort extends ProcessView with Swapping[Int] with Sorting {

  override def sort(arr: Array[Int]) {
    val swap = swapItems(arr)(_, _)

    var i = 0
    while (i < arr.size) {
      var j = i
      var k = j - 1
      while (j > 0 && (arr(j) < arr(k))) {
        swap(j, k)
        show(arr, j :: k :: Nil, TriangleMarker(j, k, i))
        j = k
        k = k - 1
      }
      i = i + 1
      show(arr, Nil, TriangleMarker(j, k, i))
    }
  }
}
