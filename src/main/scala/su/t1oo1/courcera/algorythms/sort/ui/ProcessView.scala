package su.t1oo1.courcera.algorythms.sort.ui

import java.awt.{Color, Graphics2D, Rectangle, Graphics}
import java.awt.RenderingHints
import java.awt.BasicStroke
import javax.swing.JPanel

trait ProcessView extends JPanel {

  private var values: List[Int] = Nil
  private var last: List[Int] = Nil
  private var markers: List[Marker] = Nil
  private var updatedIndices: List[Int] = Nil
  private var stopped: Boolean = true
  private var step = 0

  def show(values: Array[Int], updatedIndices: List[Int], markers: List[Marker]): Unit = {
    this.values = values.toList
    this.markers = markers
    this.updatedIndices = updatedIndices
    if (stopped) {
      step = 0
    }
    step += 1
    repaint()
    stopped = false
    Thread.sleep(stepTimeout)
  }

  def stepTimeout: Long = 200L

  def end(): Unit = {
    while (last.nonEmpty) {
      repaint()
    }
    stopped = true
    repaint()
  }

  protected override def paintComponent(g: Graphics): Unit = {
    super.paintComponent(g)
    val viewRect: Rectangle = getVisibleRect
    val g2: Graphics2D = g.asInstanceOf[Graphics2D]
    g2.setColor(Color.DARK_GRAY.darker)
    g2.fillRect(viewRect.x, viewRect.y, viewRect.width, viewRect.height)

    def drawState() {
      val lines = values.zipWithIndex
      val minValue = values.min
      val maxValue = values.max - minValue

      //draw background
      val count = lines.size
      val space = viewRect.getMaxX / (count + 1)
      val stroke = viewRect.getMaxX / (count * 2)

      def positionX(lineNum: Int): Double = {
        lineNum * space + space
      }

      def drawValue(value: (Int, Int)): Unit = {
        val v = positionX(value._2).asInstanceOf[Int]
        val maxLength = viewRect.getMaxY - (space * 2)
        val length = (maxLength / maxValue) * (value._1 - minValue)
        val l = space + maxLength - length
        g2.setStroke(new BasicStroke(stroke.asInstanceOf[Int]))
        g2.drawLine(v, l.asInstanceOf[Int], v, (viewRect.getMaxY - space).asInstanceOf[Int])
      }

      //draw all lines
      g2.setColor(Color.DARK_GRAY)
      for (i <- lines) drawValue(i)

      //draw last lines
      g2.setColor(Color.GRAY)
      val lastLines = for (i <- last) yield (values(i), i)
      for (i <- lastLines) drawValue(i)


      def createMarkerRectangle(lineNum: Int): Rectangle = {
        val middle = positionX(lineNum)
        val bound = stroke / 2
        val left = middle - bound
        val top = viewRect.getMaxY - space + bound

        new Rectangle(
          left.asInstanceOf[Int],
          top.asInstanceOf[Int],
          stroke.asInstanceOf[Int],
          stroke.asInstanceOf[Int])
      }

      g2.setColor(Color.WHITE)
      val updatedLines = for (i <- updatedIndices) yield (values(i), i)
      for (i <- updatedLines) {
        drawValue(i)
      }

      for (i <- markers) i.drawMarker(g2, createMarkerRectangle(i.index))

      last = updatedIndices
      updatedIndices = Nil
      markers = Nil
    }

    if (values.nonEmpty) drawState()

    def drawButton() {
      g.setColor(Color.WHITE)
      val maxX = getVisibleRect.getMaxX
      val maxY = getVisibleRect.getMaxY
      val bound = (if (maxX < maxY) maxX else maxY) / 10
      val left = (maxX / 2 - bound).asInstanceOf[Int]
      val right = (maxX / 2 + bound).asInstanceOf[Int]
      val top = (maxY / 2 - bound).asInstanceOf[Int]
      val bottom = (maxY / 2 + bound).asInstanceOf[Int]
      val middle = (maxY / 2).asInstanceOf[Int]
      g.fillPolygon(Array(left, left, right), Array(top, bottom, middle), 3)
    }

    if (stopped) drawButton()

    g.setColor(Color.GRAY)
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
      RenderingHints.VALUE_ANTIALIAS_ON)

    g2.drawString(s"Steps: $step, Size: ${values.size}",20,20)
  }
}
