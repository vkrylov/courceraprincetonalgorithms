package su.t1oo1.courcera.algorythms.sort.ui

import java.awt.event.{MouseEvent, MouseAdapter}
import java.awt.{BorderLayout, Dimension}
import javax.swing.JFrame

import su.t1oo1.courcera.algorythms.week2.{InsertionSort, SelectionSort, ShellSort}
import su.t1oo1.courcera.algorythms.week3.{DijkstraPartitioning, QuickSort, BottomUpMergeSort, MergeSort}

import scala.util.Random

trait MainFrame extends JFrame {
  var inAction = false

  val dashboard = new DashBoard(300, 20)

  setLayout(new BorderLayout)
  val processView = new QuickSort {
    override def stepTimeout: Long = {
      dashboard.speed
    }
  }
  setTitle(processView.getClass.getSimpleName)
  add(processView, BorderLayout.CENTER)
  add(dashboard, BorderLayout.SOUTH)
  setSize(new Dimension(400, 400))
  setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)

  processView.addMouseListener(new MouseAdapter {
    override def mouseClicked(e: MouseEvent): Unit = {
      inAction = true
    }
  })

  def run(): Unit = {
    while (true) {
      if (inAction) {
        processView.sort(data(dashboard.count).toArray)
        processView.repaint()
        processView.end()
        inAction = false
      }
      Thread.sleep(100)
    }
  }

  def data(max: Int): Seq[Int]
}
