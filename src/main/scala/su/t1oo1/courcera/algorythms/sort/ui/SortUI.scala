package su.t1oo1.courcera.algorythms.sort.ui

import scala.util.Random

object SortUI extends App {
  val frame = new MainFrame {
    override def data(max: Int): Seq[Int] = {
      randomData(max)
    }
  }
  frame.setLocationRelativeTo(null)
  frame.setVisible(true)
  frame.run()


  def randomData(max: Int): Seq[Int] = {
    val list = 1 to max
    val withRandom = for (i <- list) yield (Random.nextInt(max), i)
    withRandom.sorted.unzip._2
  }

  def sortedData(max: Int): Seq[Int] = 1 to max

  def reversedData(max: Int): Seq[Int] = max to 1 by -1

  def fewUnique(max: Int): Seq[Int] = {
    val list = 1 to max
    val withRandom = for (i <- list) yield (Random.nextInt(max), i match {
      case j if j < (max / 4) => max / 4
      case j if j < (max / 2)  => max / 2
      case j if j < (max / 4) * 3 => (max / 4) * 3
      case _ => max
    })
    withRandom.sorted.unzip._2
  }
}
