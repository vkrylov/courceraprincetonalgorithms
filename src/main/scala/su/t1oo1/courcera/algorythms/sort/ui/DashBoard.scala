package su.t1oo1.courcera.algorythms.sort.ui

import java.awt.{Dimension, Component, BorderLayout}
import javax.swing._
import javax.swing.event.{ChangeEvent, ChangeListener}

class DashBoard(val speedValue: Int, val countValue: Int) extends JPanel {

  val maxSpeedValue = 1000
  val minSpeedValue = 1

  setLayout(new BorderLayout())
  val vBox = Box.createVerticalBox()

  val speedSlider = new JSlider(minSpeedValue, maxSpeedValue)
  val speedLabel = new JLabel("Step time")
  speedLabel.setPreferredSize(new Dimension(100, 10))
  speedSlider.addChangeListener(new ChangeListener {
    override def stateChanged(e: ChangeEvent): Unit = {
      speedLabel.setText(s"Step time: $speed")
    }
  })
  speedSlider.setValue(speedValue)
  vBox.add(labeledPanel(speedLabel, speedSlider))


  val maxCount = 1000
  val minCount = 2
  val countSlider = new JSlider(minCount, maxCount)
  val countLabel = new JLabel("Count")
  countLabel.setPreferredSize(new Dimension(100, 10))
  countSlider.addChangeListener(new ChangeListener {
    override def stateChanged(e: ChangeEvent): Unit = {
      countLabel.setText(s"Count: $count")
    }
  })
  countSlider.setValue(countValue)
  vBox.add(labeledPanel(countLabel, countSlider))
  add(vBox, BorderLayout.CENTER)

  def labeledPanel(label: Component, component: Component): JPanel = {
    val panel = new JPanel(new BorderLayout())
    panel.add(label, BorderLayout.WEST)
    panel.add(component, BorderLayout.CENTER)
    panel
  }
  def speed = speedSlider.getValue
  def count = countSlider.getValue
}
