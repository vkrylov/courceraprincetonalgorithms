package su.t1oo1.courcera.algorythms.sort.ui

import java.awt.{Rectangle, Graphics2D}

trait MarkerPainter {
  def drawMarker = (g2: Graphics2D, place: Rectangle) => {println(place)}
}

trait Marker extends MarkerPainter {
  val index: Int
}

object TriangleMarker {
  val drawMarker = (g2: Graphics2D, place: Rectangle) => {
    val left = place.getX
    val right = place.getMaxX
    val middle = place.getX + (place.getWidth / 2)

    val top = place.getY
    val bottom = place.getMaxY

    g2.fillPolygon(
      Array(left.asInstanceOf[Int], middle.asInstanceOf[Int], right.asInstanceOf[Int]),
      Array(bottom.asInstanceOf[Int], top.asInstanceOf[Int], bottom.asInstanceOf[Int]),
      3)
  }

  def apply(i: Int): Marker = {
    new Marker {
      override val index: Int = i
      override val drawMarker = TriangleMarker.drawMarker
    }
  }

  def apply(indices: Int*): List[Marker] = {
    indices.map(TriangleMarker(_)).toList
  }
}

object TextMarker {
  val drawMarker = (g2: Graphics2D, place: Rectangle, t: String) => {
    val x = place.getX.asInstanceOf[Int]
    val y = place.getMaxY.asInstanceOf[Int]
    g2.drawString(t, x, y)
  }
  def apply(i: (String, Int)): TextMarker = new TextMarker(i._1, i._2)

  def apply(data: (String, Int)*): List[TextMarker] = {
    data.map(TextMarker(_)).toList
  }
}

class TextMarker(t: String, i: Int) extends Marker {
  override val index: Int = i
  override val drawMarker = (g2: Graphics2D, place: Rectangle) => {
    TextMarker.drawMarker(g2, place, t)
  }
}