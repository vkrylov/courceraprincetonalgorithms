package su.t1oo1.courcera.algorythms.week1

trait Connect {

  def union(a: Int, b: Int): Unit

  def connected(a: Int, b: Int): Boolean

  def loggedUnion(a: Int, b: Int) {
    union(a, b)
    println(s"union $a and $b [ $this ]")
  }

  def unionBySteps(steps : List[(Int, Int)]): Unit = {
    steps.foreach(s => union(s._1, s._2))
  }

  def toList: List[Int]
}
