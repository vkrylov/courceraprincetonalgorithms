package su.t1oo1.courcera.algorythms.week1

import scala.annotation.tailrec

class QuickUnion(a: Array[Int]) extends Connect {

  var arr = a.clone()

  override def union(a: Int, b: Int) {
    arr(a) = rootValue(arr(b))
  }

  override def connected(a: Int, b: Int): Boolean = {
    rootValue(a) == rootValue(b)
  }

  def rootValue(index : Int): Int = {
    @tailrec def searchRoot(i: Int): Int = {
      if (arr(i) == i) i else searchRoot(arr(i))
    }
    searchRoot(index)
  }

  override def toString: String = {
    arr mkString ", "
  }

  override def toList: List[Int] = arr.toList
}
