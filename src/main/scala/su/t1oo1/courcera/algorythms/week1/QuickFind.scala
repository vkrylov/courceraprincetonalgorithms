package su.t1oo1.courcera.algorythms.week1

class QuickFind(a: Array[Int]) extends Connect {

  var arr = a.clone

  override def union(a: Int, b: Int) {
    val from = arr(a)
    val to = arr(b)
    arr = for {
      i <- arr
    } yield if (i == from) to else i
  }

  override def connected(a: Int, b: Int): Boolean = {
    arr(a) == arr(b)
  }
  
  override def toString: String = {
    arr mkString(", ")
  }

  override def toList: List[Int] = arr.toList
}