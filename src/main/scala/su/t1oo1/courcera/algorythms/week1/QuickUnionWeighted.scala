package su.t1oo1.courcera.algorythms.week1

class QuickUnionWeighted(a: Array[Int]) extends QuickUnion(a) {

  override def union(a : Int, b: Int) {
    val aRoot = rootValue(a)
    val aTreeWeight = treeWeight(aRoot)
    val bRoot = rootValue(b)
    val bTreeWeight = treeWeight(bRoot)
    if (aTreeWeight >= bTreeWeight) arr(bRoot) = aRoot else arr(aRoot) = bRoot
  }

  def treeWeight(root: Int): Int = {
    val nodes = for {
      i <- arr
      if rootValue(i) == root
    } yield 1

    nodes.sum
  }

  override def toList: List[Int] = arr.toList
}
