package su.t1oo1.courcera.algorythms.week4

import su.t1oo1.courcera.algorythms.week2.Swapping

import scala.reflect.ClassTag

class UnorderedMaxPQ[A <% Ordered[A]](capacity: Int)(implicit m: ClassTag[A]) extends PQ[A] with Swapping[A] {
  private val pq: Array[A] = new Array[A](capacity)
  private var n = 0

  def empty(): Boolean = n == 0

  def insert(x: A): Unit = {
    pq(n) = x
    n += 1
  }

  def delMax(): A = {
    var max = 0
    for (i <- 1 until n) {
      if (pq(max) < pq(i)) max = i
    }
    swapItems(pq)(max, n - 1)
    val k = pq(n - 1)
    n -= 1
    k
  }
}
