package su.t1oo1.courcera.algorythms.week4

trait PQ[A] {

  def empty(): Boolean

  def insert(x: A): Unit

  def delMax(): A
}
